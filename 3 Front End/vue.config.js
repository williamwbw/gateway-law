module.exports = {
  publicPath: '/gwlw',
  assetsDir: 'src',
  css: {
    modules: true,
    sourceMap: true
  }
}